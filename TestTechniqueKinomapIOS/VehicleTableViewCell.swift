//
//  VehicleTableViewCell.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 24/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import UIKit

class VehicleTableViewCell: UITableViewCell {
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var vehicleImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
