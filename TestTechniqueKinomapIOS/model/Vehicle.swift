//
//  Vehicle.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 24/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import Foundation

class Vehicle : NSObject {
    
    var id : Int?
    var name : String?
    var iconUrl : String?
    
    override init() {
    }
    
    init(id : Int, name: String, iconUrl: String) {
        self.id = id;
        self.name = name;
        self.iconUrl = iconUrl;
    }
}
