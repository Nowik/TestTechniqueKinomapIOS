//
//  ViewControllerPresenter.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 24/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import Foundation
import Alamofire

class ViewControllerPresenter {
    
    func getVehicles() -> [Vehicle] {
        return NetworkManager.shared().getVehicles()
    }
}
