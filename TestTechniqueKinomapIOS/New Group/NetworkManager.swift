//
//  NetworkManager.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 25/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager {
    
    // MARK: - Properties
    
    private static var sharedNetworkManager: NetworkManager = {
        let networkManager = NetworkManager.init()
        
        return networkManager
    }()
    
    
    private init() {
    }
    
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    public func getVehicles(completion: @escaping ([Vehicle]) -> ()){
        
        var vehicleList = [Vehicle]()
        
        Alamofire.request("https://api.myjson.com/bins/160b49", method: .get).validate().responseJSON { response in
            print(response.result)
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                for (_, vehicleJson):(String, JSON) in json["vehicleList"]["response"] {
                    
                    let id = vehicleJson["id"].intValue
                    let name = vehicleJson["name"].stringValue
                    let iconUrl = vehicleJson["icon"]["url"]["left"].stringValue
                    
                    let newVehicle = Vehicle.init(id: id, name: name, iconUrl: iconUrl)
                    vehicleList.append(newVehicle)
                }
                 completion(vehicleList)
              
            case .failure(let error):
                print(error)
                completion(vehicleList)
            }
        }
    }
}
    

