//
//  ViewController.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 24/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ViewController: UIViewController {
    
    @IBOutlet weak var vehicleTableView: UITableView!
    
    let VEHICLE_DETAIL_SEGUE_ID = "vehicle_detail_segue"
    var vehicleList = [Vehicle]()
    var selectedVehicle = Vehicle()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Vehicles"
        
        NetworkManager.shared().getVehicles{ (vehicleList : [Vehicle]) in   // Object received from closure
            self.vehicleList = vehicleList
            self.vehicleTableView.reloadData()
        }
        
        DispatchQueue.main.async{
            self.vehicleTableView.reloadData()
        }
        print(self.vehicleList)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = vehicleTableView.dequeueReusableCell(withIdentifier: "vehicle_cell_id", for: indexPath) as! VehicleTableViewCell
        
        let vehicle = vehicleList[indexPath.row]
        cell.vehicleNameLabel.text = vehicle.name
        if let iconUrl = vehicle.iconUrl {
        Alamofire.request(iconUrl).responseImage { response in
                if let vehicleIcon = response.result.value {
                    cell.vehicleImageView.image = vehicleIcon
                }
            }
        }
        return cell
    }
}

extension ViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = vehicleTableView.cellForRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        selectedVehicle = vehicleList[indexPath.row]
        performSegue(withIdentifier: VEHICLE_DETAIL_SEGUE_ID, sender: cell)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == VEHICLE_DETAIL_SEGUE_ID) {
            // initialize new view controller and cast it as your view controller
            let vehicleDetailViewController = segue.destination as! VehicleDetailViewController
            // your new view controller should have property that will store passed value
            vehicleDetailViewController.selectedVehicle = selectedVehicle
        }
    }
    
}

