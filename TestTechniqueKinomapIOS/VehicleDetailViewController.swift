//
//  VehicleDetailViewController.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 25/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import UIKit
import Alamofire

class VehicleDetailViewController: UIViewController {
    
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var vehicleIconImageView: UIImageView!
    
    var selectedVehicle = Vehicle()

    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationItem.title = selectedVehicle.name
        if let iconUrl = selectedVehicle.iconUrl {
            Alamofire.request(iconUrl).responseImage { response in
                if let vehicleIcon = response.result.value {
                    self.vehicleIconImageView.image = vehicleIcon
                }
            }
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
