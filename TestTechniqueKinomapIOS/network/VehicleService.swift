//
//  VehicleService.swift
//  TestTechniqueKinomapIOS
//
//  Created by Gregory Nowik on 25/01/18.
//  Copyright © 2018 Gregory Nowik. All rights reserved.
//

import Foundation

class VehicleService {

    func getVehicles() -> Void {
        Alamofire.request("https://httpbin.org/get").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
    
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
    
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
        }
    }
}
